Mills Williams Foundation website
=================================

Site sources files for building the Mills Williams site.

Previewing and building the site
-----------------------------

Clone the source files by running:

    git clone git@bitbucket.org:woolnerpagan/mills-williams-website.git

then cd into the directory:

    cd mills-williams-website

You'll need to install [NodeJS](http://nodejs.org/). Once installed, run the following:

    npm install && bower install

This will install all dependencies. Once this has completed, run:

    grunt serve

to run a [local server](http://127.0.0.1:9000) or

    grunt build

to build the site files into /dist.
