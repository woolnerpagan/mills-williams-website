/* global require, module */
// Generated on 2014-04-02 using generator-webapp 0.4.8
'use strict';
var clone = clone || null;
// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Convert newer images
  grunt.loadNpmTasks('grunt-newer');

  // Grab Google Sheets document and convert to JSON
  grunt.loadNpmTasks('grunt-wget');

  // Generate static files from JSON
  grunt.loadNpmTasks('grunt-compile-handlebars');

  // Convert images to responsive formats
  grunt.loadNpmTasks('grunt-responsive-images');

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    config: {
      // Configurable paths
      app: 'app',
      dist: 'dist'
    },

    wget: {
      options: {
        overwrite: true
      },
      dist: {
        src: 'https://spreadsheets.google.com/feeds/list/12ZwCwQW4XWaR3qye_iuB5jjXPHXunbwEbfDdld0ayQk/1/public/values?alt=json',
        dest: '.tmp/data.json'
      }
    },

    uncss: {
      dist: {
        options: { stylesheets: ['../.tmp/styles/main.css'] },
        files: { '.tmp/styles/main.css': ['<%= config.app %>/index.html'] }
      }
    },

    'compile-handlebars': {
      index: {
        template: '<%= config.app %>/templates/index.hbs',
        templateData: '.tmp/data.json',
        output: '<%= config.app %>/index.html'
      },
      imgCss: {
        template: '<%= config.app %>/templates/backgrounds.hbs',
        templateData: '.tmp/data.json',
        output: '<%= config.app %>/styles/backgrounds.scss'
      }
    },

    'responsive_images': {
      profiles: {
        options: {
          force: true,
          separator: '-',
          engine: 'gm',
          sizes: [
            { 'name': 'xs', 'width': 320, upscale: true, quality: 50 },
            { 'name': 'sm', 'width': 992, upscale: true, quality: 85 },
            { 'name': 'sm-2x', 'width': 992*2, upscale: true, quality: 85 },
            { 'name': 'md', 'width': 1200, upscale: true, quality: 75 },
            { 'name': 'md-2x', 'width': 1200*2, upscale: true, quality: 75 },
            { 'name': 'lg', 'width': 1680, upscale: true, quality: 60 },
            { 'name': 'lg-2x', 'width': 1680*2, upscale: true, quality: 60 }
          ]
        },
        files: [{
          expand: true,
          cwd: '<%= config.app %>/images/src-profiles/',
          src: ['*.jpg'],
          dest: '<%= config.app %>/images/profiles/'
        }]
      },
      thumbs: {
        options: {
          separator: '-',
          engine: 'im',
          sizes: [
            { 'name': 'xs', 'width': 160, upscale: true, quality: 50 },
            { 'name': 'sm', 'width': 420, upscale: true, quality: 80 },
            { 'name': 'sm-2x', 'width': 420*2, upscale: true, quality: 80 },
            { 'name': 'lg', 'width': 768, upscale: true, quality: 70 },
            { 'name': 'lg-2x', 'width': 768*2, upscale: true, quality: 70 }
          ]
        },
        files: [{
          expand: true,
          cwd: '<%= config.app %>/images/src-thumbs/',
          src: ['*.jpg'],
          dest: '<%= config.app %>/images/thumbs/'
        }]
      }
    },

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['bowerInstall']
      },
      js: {
        files: ['<%= config.app %>/scripts/{,*/}*.js'],
        tasks: ['jshint'],
        options: {
          livereload: true
        }
      },
      jstest: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['test:watch']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      handlebarsIndex: {
        files: ['<%= config.app %>/templates/index.hbs'],
        tasks: ['compile-handlebars:index']
      },
      handlebarsBgs: {
        files: ['<%= config.app %>/templates/backgrounds.hbs'],
        tasks: ['compile-handlebars:imgCss']
      },
      images: {
        files: ['<%= config.app %>/images/src*/*.*'],
        tasks: ['responsive_images']
      },
      styles: {
        files: ['<%= config.app %>/styles/{,*/}*.css'],
        tasks: ['newer:copy:styles', 'autoprefixer']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= config.app %>/{,*/}*.html',
          '.tmp/styles/{,*/}*.css',
          '<%= config.app %>/images/{,*/}*'
        ]
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 3000,
        livereload: 35729,
        // Change this to '0.0.0.0' to access the server from outside
        hostname: '0.0.0.0'
      },
      livereload: {
        options: {
          open: false,
          base: [
            '.tmp',
            '<%= config.app %>'
          ]
        }
      },
      test: {
        options: {
          port: 9001,
          base: [
            '.tmp',
            'test',
            '<%= config.app %>'
          ]
        }
      },
      dist: {
        options: {
          open: true,
          base: '<%= config.dist %>',
          livereload: false
        }
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp/**',
            '.tmp',
            '<%= config.app %>/images/{thumbs,profiles}/*',
            '<%= config.dist %>/*',
            '!<%= config.dist %>/.git*'
          ]
        }]
      },
      server: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= config.app %>/images/{thumbs,profiles}/*',
            '<%= config.app %>/index.html'
          ]
        }]
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: [
        'Gruntfile.js',
        '<%= config.app %>/scripts/{,*/}*.js',
        '!<%= config.app %>/scripts/vendor/*',
        'test/spec/{,*/}*.js'
      ]
    },

    // Mocha testing framework configuration options
    mocha: {
      all: {
        options: {
          run: true,
          urls: ['http://<%= connect.test.options.hostname %>:<%= connect.test.options.port %>/index.html']
        }
      }
    },

    // Compiles Sass to CSS and generates necessary files if requested
    compass: {
      options: {
        sassDir: '<%= config.app %>/styles',
        cssDir: '.tmp/styles',
        generatedImagesDir: '.tmp/images/generated',
        imagesDir: '<%= config.app %>/images',
        javascriptsDir: '<%= config.app %>/scripts',
        fontsDir: '<%= config.app %>/styles/fonts',
        importPath: '<%= config.app %>/bower_components',
        httpImagesPath: '/images',
        httpGeneratedImagesPath: '/images/generated',
        httpFontsPath: '/styles/fonts',
        relativeAssets: false,
        assetCacheBuster: false
      },
      dist: {
        options: {
          generatedImagesDir: '<%= config.dist %>/images/generated'
        }
      },
      serverPre: {
        options: {
          debugInfo: true,
        }
      },
      server: {
        options: {
          debugInfo: true,
          watch: true,
        }
      }
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['last 1 version']
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      }
    },

    // Automatically inject Bower components into the HTML file
    bowerInstall: {
      app: {
        src: ['<%= config.app %>/index.html'],
        ignorePath: '<%= config.app %>/',
        exclude: ['<%= config.app %>/bower_components/bootstrap-sass/vendor/assets/javascripts/bootstrap.js']
      },
      sass: {
        src: ['<%= config.app %>/styles/{,*/}*.{scss,sass}'],
        ignorePath: '<%= config.app %>/bower_components/'
      }
    },

    // Renames files for browser caching purposes
    rev: {
      dist: {
        files: {
          src: [
            '<%= config.dist %>/scripts/{,*/}*.js',
            '<%= config.dist %>/styles/{,*/}*.css',
            '<%= config.dist %>/images/{,*/}*.*',
            '<%= config.dist %>/audio/{,*/}*.*',
            '<%= config.dist %>/styles/fonts/{,*/}*.*',
            '<%= config.dist %>/*.{ico,png}'
          ]
        }
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      options: {
        dest: '<%= config.dist %>'
      },
      html: '<%= config.app %>/index.html'
    },

    // Performs rewrites based on rev and the useminPrepare configuration
    usemin: {
      options: {
        assetsDirs: ['<%= config.dist %>', '<%= config.dist %>/images', '<%= config.dist %>/audio'],
        patterns: {
          audio: [
            [/<audio[^>]+src=['"]([^"']+)["']/gm,'Update the HTML with the new audio filenames']
          ]
        }
      },
      html: ['<%= config.dist %>/{,*/}*.html'],
      audio: ['<%= config.dist %>/{,*/}*.html'],
      css: ['<%= config.dist %>/styles/{,*/}*.css']
    },

    // The following *-min tasks produce minified files in the dist folder
    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/images',
          src: '{profiles,thumbs}/*.{gif,jpeg,jpg,png}',
          dest: '<%= config.dist %>/images'
        }],
      },
      favicons: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>',
          src: '*.{gif,jpeg,jpg,png}',
          dest: '<%= config.dist %>'
        }]
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/images',
          src: '{,*/}*.svg',
          dest: '<%= config.dist %>/images'
        }]
      }
    },

    htmlmin: {
      dist: {
        options: {
          collapseBooleanAttributes: true,
          collapseWhitespace: true,
          removeAttributeQuotes: true,
          removeCommentsFromCDATA: true,
          removeEmptyAttributes: true,
          removeOptionalTags: true,
          removeRedundantAttributes: true,
          useShortDoctype: true
        },
        files: [{
          expand: true,
          cwd: '<%= config.dist %>',
          src: '{,*/}*.html',
          dest: '<%= config.dist %>'
        }]
      }
    },

    // By default, your `index.html`'s <!-- Usemin block --> will take care of
    // minification. These next options are pre-configured if you do not wish
    // to use the Usemin blocks.
    // cssmin: {
    //   dist: {
    //     files: {
    //       '<%= config.dist %>/styles/main.css': [
    //         '.tmp/styles/{,*/}*.css',
    //         '<%= config.app %>/styles/{,*/}*.css'
    //       ]
    //     }
    //   }
    // },
    // uglify: {
    //   dist: {
    //     files: {
    //       '<%= config.dist %>/scripts/scripts.js': [
    //         '<%= config.dist %>/scripts/scripts.js'
    //       ]
    //     }
    //   }
    // },
    // concat: {
    //   dist: {}
    // },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.app %>',
          dest: '<%= config.dist %>',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            'images/{,*/}*.webp',
            'audio/*.mp3',
            '{,*/}*.html',
            'styles/fonts/{,*/}*.*',
            'bower_components/bootstrap-sass-official/vendor/assets/fonts/bootstrap/*.*'
          ]
        }]
      },
      styles: {
        expand: true,
        dot: true,
        cwd: '<%= config.app %>/styles',
        dest: '.tmp/styles/',
        src: '{,*/}*.css'
      }
    },

    // Generates a custom Modernizr build that includes only the tests you
    // reference in your app
    modernizr: {
      dist: {
        devFile: '<%= config.app %>/bower_components/modernizr/modernizr.js',
        outputFile: '<%= config.dist %>/scripts/vendor/modernizr.js',
        files: {
            src: [
              '<%= config.dist %>/scripts/{,*/}*.js',
              '<%= config.dist %>/styles/{,*/}*.css',
              '!<%= config.dist %>/scripts/vendor/*'
            ]
        },
        uglify: true
      }
    },

    // Run some tasks in parallel to speed up build process
    concurrent: {
      server: [
        'compass:serverPre',
        'copy:styles',
        'concurrent:responsiveImages'
      ],
      serverWatch: {
        tasks: [
          'compass:server',
          'watch'
        ],
        options: {
          logConcurrentOutput: true
        }
      },
      responsiveImages: {
        tasks: [
          'responsive_images:profiles',
          'responsive_images:thumbs'
        ]
      },
      handlebars: {
        tasks: [
          'compile-handlebars:index',
          'compile-handlebars:imgCss'
        ],
        options: {
          logConcurrentOutput: true
        }
      },
      test: [
        'copy:styles'
      ],
      dist: [
        'compass:dist',
        'copy:styles',
        'imagemin',
        'svgmin'
      ]
    }
  });

  grunt.registerTask('parseData', function () {
    var dataFile = '.tmp/data.json',
        count = 1,
        newAwards = [],
        data, newData, total = 0,
        lastName;
    if (!grunt.file.exists(dataFile)) {
      grunt.log.error('file ' + dataFile + ' not found');
      return false;
    }
    data = grunt.file.readJSON(dataFile);
    newData = {
      winners: [],
      awards: {
        'All winners': {
          items: [],
          id: 0,
          active: true
        }
      }
    };
    data.feed.entry.forEach(function (v,k) {
      newData.winners.push({
        name: v.gsx$name.$t,
        year: v.gsx$year.$t,
        award: v.gsx$award.$t || 'NA',
        photo: v.gsx$photo.$t || 'placeholder',
        audio: v.gsx$audio.$t,
        teaser: v.gsx$teaser.$t,
        body: v.gsx$body.$t,
        website: v.gsx$website.$t,
        id: k + 1,
        clearTwo: k % 2 === 1 ? true : false,
        clearThree: k % 3 === 2 ? true : false,
        prev: lastName || false
      });
      lastName = v.gsx$name.$t;
    });
    newData.winners.forEach(function (v,k) {
      newData.winners[k].next = newData.winners[k+1] ? newData.winners[k+1].name : false;
      newData.awards['All winners'].items.push(clone(v));
      if (!newData.awards[v.award]) {
        newData.awards[v.award] = {
          'items': [],
          'id': count
        };
        count++;
      }
      total = newData.awards[v.award].items.length;
      v.clearTwo = total % 2 === 1 ? true : false;
      v.clearThree = total % 3 === 2 ? true : false;
      newData.awards[v.award].items.push(v);
    });
    var awardKeys = ['All winners', 'RCM', 'Purcell', 'RAM', 'Commission', 'RNCM'];
    awardKeys.forEach(function (key) {
      newData.awards[key].name = key;
      newAwards.push(newData.awards[key]);
    });
    newData.awards = newAwards;
    grunt.file.write(dataFile, JSON.stringify(newData, null, 2));
  });

  grunt.registerTask('serve', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'clean:server',
      'wget:dist',
      'parseData',
      'concurrent:handlebars',
      'concurrent:server',
      'autoprefixer',
      'connect:livereload',
      'concurrent:serverWatch'
    ]);
  });

  grunt.registerTask('server', function (target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run([target ? ('serve:' + target) : 'serve']);
  });

  grunt.registerTask('test', function (target) {
    if (target !== 'watch') {
      grunt.task.run([
        'clean:server',
        'concurrent:test',
        'autoprefixer'
      ]);
    }

    grunt.task.run([
      'connect:test',
      'mocha'
    ]);
  });

  grunt.registerTask('build', [
    'clean:dist',
    'wget:dist',
    'parseData',
    'concurrent:handlebars',
    'useminPrepare',
    'concurrent:responsiveImages',
    'concurrent:dist',
    /*'uncss',*/
    'autoprefixer',
    'concat',
    'cssmin',
    'uglify',
    'copy:dist',
    'modernizr',
    'rev',
    'usemin',
    'htmlmin'
  ]);

  grunt.registerTask('default', [
    'newer:jshint',
    'test',
    'build'
  ]);
};

clone = function (obj) {
  if (null === obj || 'object' !== typeof obj) {
    return obj;
  }
  var copy = obj.constructor();
  for (var attr in obj) {
    if (obj.hasOwnProperty(attr)) {
      copy[attr] = obj[attr];
    }
  }
  return copy;
};

