'use strict';

var History = window.History || null,
    MediaElement = window.MediaElement || null,
    mw = window.mw || null;

(function ($, MediaElement, History, window) {

  mw = {
  
    audioObj: null,

    init: function () {
      mw.initDom();
      mw.initOverview();
      mw.initCarousel();
      mw.initAudioControls();
    },

    initDom: function () {
      mw.carousel = $('#carousel');
      mw.headerH = $('.main').height();
    },

    initOverview: function () {
      var dropdown = $('#overview-container .overview-filter'),
          ddText = $('#overview-container h2');
      $('li a', dropdown).on('show.bs.tab', function () {
        ddText.text($(this).text());
        $(this)
          .parent()
          .addClass('active')
          .siblings()
          .removeClass('active');
      });
    },

    initCarousel: function () {
      mw.checkHistoryState();
      mw.carousel
        .on('slide.bs.carousel', mw.carouselSlide)
        .on('slid.bs.carousel', mw.carouselSlid);
      History.Adapter.bind(window, 'statechange', mw.checkHistoryState);
      $('.fill-content, .fill-header', mw.carousel).each(function () {
        $(this).swipe({
          swipeLeft: function() { mw.carousel.carousel('next'); },
          swipeRight: function() { mw.carousel.carousel('prev'); }
        });
      });
    },

    initAudioControls: function () {
      $('.audio-container').each(function () {
        var self = $(this),
            controls = $('.audio-controls', self);
        new MediaElement(self.attr('data-audio-id'), {
          pluginPath: 'bower_components/mediaelement/build/',
          success: function (mediaElement) {
            $(mediaElement)
          /*.on('timeupdate', mw.update(progress))*/
              .on('play', mw.play)
              .on('pause', mw.stop);
          },
          error: function (e) {
            console.log(e);
          }
        });
        controls.click(function () {
          if (!mw.audioObj) {
            mw.audioObj = document.getElementById(self.attr('data-audio-id'));
          }
          if (mw.audioObj.paused === true) {
            mw.audioObj.play();
          } else if (mw.audioObj.paused === false) {
            mw.audioObj.pause();
          } else if (mw.audioObj.ended) {
            mw.audioObj.setCurrentTime(0);
            mw.audioObj.play();
          }
        });
      });
    },

    checkHistoryState: function () {
      var state = History.getState(), n;
      if (typeof state.data.winner !== 'undefined') {
        n = state.data.winner;
      } else if (window.location.search) {
        n = window.location.search.substring(1);
      }
      if (n) {
        mw.carousel.carousel(parseInt(n));
      }
    },

    carouselSlide: function () {
      $('html,body').animate({scrollTop:mw.headerH}, 400);
      if (mw.audioObj) {
        mw.audioObj.stop();
        mw.audioObj = null;
      }
    },

    carouselSlid: function () {
      var index = mw.carousel.data('bs.carousel').getActiveIndex(),
          winnerLinks = $('header.header .dropdown-menu li');
      winnerLinks.removeClass('active');
      if (index > 0) {
        winnerLinks
          .eq(index-1)
          .addClass('active');
      }
      if (index > 0) {
        History.pushState({winner:index}, 'Mills Williams: '+winnerLinks.eq(index-1).text(), '?'+index);
      } else {
        History.pushState({winner:index}, 'Mills Williams Foundation', '?'+index);
      }
    },

    update: function (progress) {
      return function (obj) {
        var perc = obj.target.currentTime / obj.target.duration * 100;
        progress.css({width:perc+'%'});
      };
    },

    play: function (obj) {
      var ele = $(obj.target).parents('.audio-container'),
          controls = $('.audio-controls', ele);
      ele.addClass('playing');
      controls
        .removeClass('btn-success btn-primary')
        .addClass('btn-warning');
    },

    stop: function (obj) {
      var ele = $(obj.target).parents('.audio-container'),
          controls = $('.audio-controls', ele);
      ele.removeClass('playing');
      controls
        .removeClass('btn-primary btn-warning')
        .addClass('btn-success');
    }

  };

  $(document).ready(function () {
    mw.init();
  });

})(jQuery, MediaElement, History, window);
